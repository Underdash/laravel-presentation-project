<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style type="text/css">
    	.container{
    		margin-top: 7rem;
    	}
    	.container h1{
    		margin:3.5rem 0;
    	}
    	ul{
    		list-style: none
    	}
  		li{
  			display: inline-block;
  			margin: 1px 10px;
  		}
    </style>
    <title>Tasks - {{$Todo->title}}</title>
  </head>
  <body>
  	<div class="container">
  		<h1>Your Daily Todo List</h1>
  		<nav>
  			<ul>
  				<li><a href="/"> Home </a></li>
  				<li><a href="/about"> About </a></li>
  				<li><a href="/Task"> Tasks </a></li>
  			</ul>
  	‍	</nav>
      <h2>Task #{{$Todo->id}}: {{$Todo->title}}</h2>
      <p>Notes: {{$Todo->description}}</p>
	</div>
  </body>
</html>
