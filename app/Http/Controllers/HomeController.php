<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $req){
    	return view("index");
    }
    public function about(Request $req){
    	return view("about");
    }
}
