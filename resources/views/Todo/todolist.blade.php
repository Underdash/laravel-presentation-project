<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style type="text/css">
    	.container{
    		margin-top: 7rem;
    	}
    	.container h1{
    		margin:3.5rem 0;
    	}
    	ul{
    		list-style: none
    	}
  		li{
  			display: inline-block;
  			margin: 1px 10px;
  		}
    </style>
    <title>Tasks - Todo List</title>
  </head>
  <body>
  	<div class="container">
  		<h1>Your Daily Todo List</h1>
  		<nav>
  			<ul>
  				<li><a href="/"> Home </a></li>
  				<li><a href="/about"> About </a></li>
  				<li><a href="/Task"> Tasks </a></li>
  			</ul>
  	‍	</nav>
		<table class="table">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Title</th>
					<th scope="col">isDone</th>
					<th scope="col">Options</th>
				</tr>
			</thead>
			<tbody>
				@foreach($Todos as $Todo)
					<tr>
						<th scope="row">{{$Todo->id}}</th>
						<td>{{$Todo->title}}</td>
						<td>
							@if($Todo->isDone)
								Yes
							@else
								No
							@endif
						</td>
						<td>
							<a href="/Task/{{$Todo->id}}">Show</a>
							@if($Todo->isDone)
								<a href="/Task/{{$Todo->id}}/undone">Undone</a>
							@else
								<a href="/Task/{{$Todo->id}}/done">Done</a>
							@endif
							
							<a href="/Task/{{$Todo->id}}/delete">Delete</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
  </body>
</html>
