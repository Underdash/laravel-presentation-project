<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style type="text/css">
    	.container{
    		margin-top: 7rem;
    	}
    	.container h1{
    		margin:3.5rem 0;
    	}
    	ul{
    		list-style: none
    	}
  		li{
  			display: inline-block;
  			margin: 1px 10px;
  		}
    </style>
    <title>Tasks - About us </title>
  </head>
  <body>
  	<div class="container">
  		<h1>Application's About Page</h1>
  		<nav>
  			<ul>
  				<li><a href="/"> Home </a></li>
  				<li><a href="/about"> About </a></li>
  				<li><a href="/Task"> Tasks </a></li>
  			</ul>
  	‍	</nav>
  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
  		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>
  </body>
</html>
