<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/","App\Http\Controllers\HomeController@index");
Route::get("/about","App\Http\Controllers\HomeController@about");

Route::get("/Task","App\Http\Controllers\TaskController@index");
Route::get("/Task/{id}","App\Http\Controllers\TaskController@show");
Route::get("/Task/{id}/delete","App\Http\Controllers\TaskController@delete");
Route::get("/Task/{id}/done","App\Http\Controllers\TaskController@done");
Route::get("/Task/{id}/undone","App\Http\Controllers\TaskController@undone");