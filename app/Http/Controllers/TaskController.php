<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;

class TaskController extends Controller
{
	public function index(Request $req){
		return view("Todo.todolist",["Todos"=>Todo::all()]);
	}
	public function show(Request $req,$id){
		return view("Todo.todoshow",["Todo" => Todo::findOrFail($id)]);
	}

	public function done(Request $req,$id){
		$Todo = Todo::find($id);
		$Todo->isDone = true;
		$Todo->save();
		return back();
	}

	public function delete(Request $req,$id){
		Todo::find($id)->delete();
		return back();
	}

	public function undone(Request $req, $id){
		$Todo = Todo::find($id);
		$Todo->isDone = false;
		$Todo->save();
		return back();
	}
}
